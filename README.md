# Тестовое задание "Фабрика решений"

## Установка

```bash
git clone https://gitlab.com/berzezek/fr.git

cd fr

# Создайте в корне каталога папку для логов
mkdir backend/logs

# Создайте 3 файла для логов
touch backend/logs/debug.log backend/logs/newsletters.log backend/logs/test.log
```

## Запуск

```bash
docker compose up -d --build
```

Проект будет доступен по адресу http://localhost:3000

## Остановка

```bash
docker compose down
```
